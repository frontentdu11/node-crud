var mysql = require('mysql');
module.exports = appMysql =(()=>{
    var con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "rhp"
      });
      
      con.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
      });
      /*
      var sql = 'INSERT INTO customers (name, address) VALUES ?';
      con.query(sql, [adr], function (err, result) {
      */
    function addData(sql,data){
         return new Promise((resolve,reject)=>{
            con.query(sql, [[Object.values(data)]], function (err, result) {
                if (err) reject(err);
                resolve(result)
              })
         })
    }
    function editData(sql,data){
        return new Promise((resolve,reject)=>{
           con.query(sql, Object.values(data), function (err, result) {
               if (err) reject(err);
               resolve(result)
             })
        })
   }
   function deleteData(sql,id){
    return new Promise((resolve,reject)=>{
        con.query(sql, [id], function (err, result) {
            if (err) reject(err);
            resolve(result)
          })
     })
   }
   function getDetail(sql,id){
    return new Promise((resolve,reject)=>{
        con.query(sql, [id],function (err, result) {
            if (err) reject(err);
            resolve(result)
          } )
     })
   }
   function getAll(sql){
    return new Promise((resolve,reject)=>{
        con.query(sql, function (err, result) {
            if (err) reject(err);
            resolve(result)
          })
     })
   }
    return{
        addData,
        editData,
        deleteData,
        getDetail,
        getAll
    }
})()
