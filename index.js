const express = require('express')
const bodyParser = require('body-parser');
const product = require('./model/product');
const appMysql = require('./mysql/mysql')
const app = express()
const port = 3000

app.use(bodyParser.json());

app.get('/', (req, res) => {
  const sql = 'SELECT * FROM product';
  appMysql.getAll(sql,req.params.id).then((response)=>{
    res.send({status:200,data: response})
  }).catch((err)=>{
    res.send(err)
  })
})

app.get('/product/:id', (req, res) => {
  // res.send(product.getDetail(req.params.id))
  const sql = 'SELECT * FROM product WHERE id=?';
  appMysql.getDetail(sql,req.params.id).then((response)=>{
    res.send({status:200,data:response})
  }).catch((err)=>{
    res.send(err)
  })
})

app.post('/product/add', (req, res) => {
  const sql = 'INSERT INTO product(name, price) VALUES ?';
  appMysql.addData(sql,req.body).then((response)=>{
    res.send({status:200,data:response})
  }).catch((err)=>{
    res.send(err)
  })
})

app.put('/product/:id', (req, res) => {
  const sql = 'update product SET name=? ,price=? WHERE id=?';

  appMysql.editData(sql,{...req.body, id: req.params.id}).then((response)=>{
    res.send({status:200,data:response})
  }).catch((err)=>{
    res.send(err)
  })
  // if(product.edit(req.params.id,req.body)){
  //   res.send('update success')
  // }else{
  //   res.send('update fail')
  // }
})

app.delete('/product/:id', (req, res) => {
  const sql = 'DELETE FROM product WHERE id=? ';
  appMysql.deleteData(sql,req.params.id).then(()=>{
    res.send({status:200,data:'success'})
  }).catch((err)=>{
    res.send(err)
  })
  // if(product.delete(req.params.id)){
  //   res.send('delete success')
  // }else{
  //   res.send('delete fail')
  // }
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
