
module.exports = (()=> {
    let dataProduct = [{id:'1',name:"king",price: 1000.12}]

   function getAll(){
        return dataProduct;
    }
    function save(product) {
        dataProduct.push({id:`${Date.now()}`,...product})
    }
    function edit(id, data){
        const result = findId(id);
        if(result >-1){
            dataProduct[result]= {id,...data}
            return true
        }else{
            return false
        }

    }
    function  deleteProduct(id){
        const result = findId(id);
        if(result >-1){
          dataProduct.splice(result,1)
            return true
        }else{
            return false
        }
    }
    function findId(id){
        return dataProduct.findIndex(item=> item.id === id)
    }
    function getDetail(id){
        const result = findId(id);
        if(result >-1){
        
            return dataProduct[result]
        }else{
            return null
        }
    }
    return {
        getAll: getAll,
        save: save,
        edit: edit,
        delete: deleteProduct,
        getDetail: getDetail
    }
})()